from typing import List


class IntcodeComputer:
    def __init__(self, program: List[int]):
        self.inputs: List[int] = []
        self.input_ptr = 0
        self.program: List[int] = program.copy()
        self.pc: int = 0
        self.out: int = 0
        self.finished: bool = False
        self.relative_base: int = 0

    def add_inputs(self, *values: int) -> None:
        self.inputs += values

    def read_address(self, address: int) -> int:
        if address >= len(self.program):
            self.program += [0 for i in range(address - len(self.program) + 1)]

        return self.program[address]

    def read_param(self, address: int, mode: int, output: bool = False) -> int:
        return_address: int
        if output and mode == 1:
            mode = 0
        if mode == 0:
            return_address = self.read_address(address)
        elif mode == 1:
            return_address = address
        elif mode == 2:
            return_address = self.read_address(address) + self.relative_base
        else:
            raise ValueError(f"Mode {mode} not recongnized")

        if output:
            return return_address
        return self.read_address(return_address)

    def write_address(self, address: int, value: int) -> None:
        if address >= len(self.program):
            self.program += [0 for i in range(address - len(self.program) + 1)]

        self.program[address] = value

    def run(self) -> int:
        while self.program[self.pc] != 99:
            opcode: str = f"{self.program[self.pc]:05d}"
            instruction: int = int(opcode[-2:])
            modes: List[int] = [int(x) for x in opcode[:-2]][::-1]

            # print(opcode, instruction, modes)
            num_params: int
            num_outputs: int
            if instruction in [1, 2, 7, 8]:
                num_params = 2
                num_outputs = 1
            elif instruction == 3:
                num_params = 0
                num_outputs = 1
            elif instruction in [4, 9]:
                num_params = 1
                num_outputs = 0
            elif instruction in [5, 6]:
                num_params = 2
                num_outputs = 0

            params: List[int] = [
                self.read_param(self.pc + i + 1, modes[i])
                for i in range(num_params)
            ]
            outputs: List[int] = [
                self.read_param(self.pc + num_params + i + 1,
                                modes[num_params + i],
                                output=True) for i in range(num_outputs)
            ]

            branch: bool = False
            is_output_available: bool = False

            if instruction == 1:
                self.write_address(outputs[0], params[0] + params[1])
            elif instruction == 2:
                self.write_address(outputs[0], params[0] * params[1])
            elif instruction == 3:
                if self.input_ptr >= len(self.inputs):
                    return 1
                self.write_address(outputs[0], self.inputs[self.input_ptr])
                self.input_ptr += 1
            elif instruction == 4:
                self.out = params[0]
                is_output_available = True
            elif instruction == 5:
                if params[0] != 0:
                    branch = True
            elif instruction == 6:
                if params[0] == 0:
                    branch = True
            elif instruction == 7:
                self.write_address(outputs[0],
                                   1 if params[0] < params[1] else 0)
            elif instruction == 8:
                self.write_address(outputs[0],
                                   1 if params[0] == params[1] else 0)
            elif instruction == 9:
                self.relative_base += params[0]

            if not branch:
                self.pc += 1 + num_params + num_outputs
            else:
                self.pc = params[1]

            if is_output_available:
                return 2

        self.finished = True
        return 0
