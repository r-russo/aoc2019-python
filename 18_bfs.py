from collections import deque
from dataclasses import dataclass, field
from typing import Optional


@dataclass(frozen=True)
class Position:
    x: int
    y: int
    keys: str = ""


@dataclass(frozen=True)
class Bots:
    x: tuple[int, ...]
    y: tuple[int, ...]
    keys: str = ""


class Map:
    def __init__(self, map_str: str):
        self.map = [list(x) for x in map_str.strip().splitlines()]
        self.height = len(self.map)
        self.width = len(self.map[0])
        self.keys = {x for x in map_str.strip() if x.islower()}

    def get_cell(self, x: int, y: int) -> str:
        if 0 <= x < self.width and 0 <= y < self.height:
            return self.map[y][x]
        return "#"

    def set_cell(self, x: int, y: int, shape: str) -> None:
        if len(shape) != 1:
            raise ValueError("Should be one character")
        if 0 <= x < self.width and 0 <= y < self.height:
            self.map[y][x] = shape
        else:
            raise IndexError("Coordinate not found")

    def find_cell(self, c: str) -> list[tuple[int, int]]:
        result: list[tuple[int, int]] = []
        for i, row in enumerate(self.map):
            matches = [(j, i) for j, x in enumerate(row) if x == c]
            if matches:
                result.extend(matches)

        return result

    def find_first_cell(self, c: str) -> Optional[tuple[int, int]]:
        for i, row in enumerate(self.map):
            matches = [j for j, x in enumerate(row) if x == c]
            if matches:
                return (matches[0], i)

        return None

    def get_available_neighbours(self, x: int, y: int,
                                 keys: str) -> list[tuple[int, int, str]]:
        up = (x, y - 1)
        right = (x + 1, y)
        down = (x, y + 1)
        left = (x - 1, y)

        available: list[tuple[int, int, str]] = []

        for (nx, ny) in (up, right, down, left):
            cell = self.get_cell(nx, ny)
            if cell in (".", "@"):
                available.append((nx, ny, ""))
            elif cell.isupper() and cell.lower() in keys:
                available.append((nx, ny, ""))
            elif cell.islower():
                if cell not in keys:
                    available.append((nx, ny, cell))
                else:
                    available.append((nx, ny, ""))

        return available

    def __str__(self) -> str:
        return "\n".join(["".join(row) for row in self.map])

    def __repr__(self) -> str:
        return str(self)


def bfs(maze: Map) -> int:
    visited: set[Position] = set()
    starting_position = maze.find_first_cell("@")
    if starting_position is None:
        raise IndexError("Starting position doesn't exist?")
    queue: deque[Position] = deque()
    p = Position(*starting_position)
    queue.appendleft(p)
    visited.add(p)
    path = {}

    while queue:
        v = queue.pop()
        if len(v.keys) == len(maze.keys):
            break

        for (x, y, key) in maze.get_available_neighbours(v.x, v.y, v.keys):
            new_keys = "".join(sorted(v.keys + key))
            new_p = Position(x, y, new_keys)
            if new_p not in visited:
                visited.add(new_p)
                path[new_p] = v
                queue.appendleft(new_p)

    steps: int = 1
    while path[v] != p:
        steps += 1
        v = path[v]

    return steps


def bfs_bots(maze: Map, bots_position: list[tuple[int, int]]) -> int:
    visited: set[Bots] = set()
    queue: deque[Bots] = deque()

    sx: list[int] = []
    sy: list[int] = []
    for pos in bots_position:
        sx.append(pos[0])
        sy.append(pos[1])

    p = Bots(tuple(sx), tuple(sy))
    queue.appendleft(p)
    visited.add(p)
    path = {}

    while queue:
        v = queue.pop()
        if len(v.keys) == len(maze.keys):
            break

        for i, (bx, by) in enumerate(zip(v.x, v.y)):
            for (x, y, key) in maze.get_available_neighbours(bx, by, v.keys):
                new_keys = "".join(sorted(v.keys + key))
                new_x = list(v.x)
                new_x[i] = x
                new_y = list(v.y)
                new_y[i] = y
                new_b = Bots(tuple(new_x), tuple(new_y), new_keys)
                if new_b not in visited:
                    visited.add(new_b)
                    path[new_b] = v
                    queue.appendleft(new_b)

    steps: int = 1
    while path[v] != p:
        steps += 1
        v = path[v]

    return steps


def test_cases() -> None:
    input_txt = """#########
#b.A.@.a#
#########"""
    assert bfs(Map(input_txt)) == 8

    input_txt = """########################
#f.D.E.e.C.b.A.@.a.B.c.#
######################.#
#d.....................#
########################"""
    assert bfs(Map(input_txt)) == 86

    input_txt = """########################
#...............b.C.D.f#
#.######################
#.....@.a.B.c.d.A.e.F.g#
########################"""
    assert bfs(Map(input_txt)) == 132

    input_txt = """#################
#i.G..c...e..H.p#
########.########
#j.A..b...f..D.o#
########@########
#k.E..a...g..B.n#
########.########
#l.F..d...h..C.m#
#################"""
    assert bfs(Map(input_txt)) == 136

    input_txt = """########################
#@..............ac.GI.b#
###d#e#f################
###A#B#C################
###g#h#i################
########################"""
    assert bfs(Map(input_txt)) == 81

    input_txt = """#######
#a.#Cd#
##@#@##
#######
##@#@##
#cB#Ab#
#######"""
    m = Map(input_txt)
    assert bfs_bots(m, m.find_cell("@")) == 8

    input_txt = """###############
#d.ABC.#.....a#
######@#@######
###############
######@#@######
#b.....#.....c#
###############"""
    m = Map(input_txt)
    assert bfs_bots(m, m.find_cell("@")) == 24

    input_txt = """#############
#DcBa.#.GhKl#
#.###@#@#I###
#e#d#####j#k#
###C#@#@###J#
#fEbA.#.FgHi#
#############"""
    m = Map(input_txt)
    assert bfs_bots(m, m.find_cell("@")) == 32

    input_txt = """#############
#g#f.D#..h#l#
#F###e#E###.#
#dCba@#@BcIJ#
#############
#nK.L@#@G...#
#M###N#H###.#
#o#m..#i#jk.#
#############"""
    m = Map(input_txt)
    assert bfs_bots(m, m.find_cell("@")) == 72


def part1(input_txt: str) -> int:
    return bfs(Map(input_txt))


def part2(input_txt: str) -> int:
    m = Map(input_txt)
    starting_position = m.find_first_cell("@")
    if starting_position is None:
        raise IndexError("Starting position doesn't exist?")
    sx, sy = starting_position

    for i in range(-1, 2):
        for j in range(-1, 2):
            if i in (-1, 1) and j in (-1, 1):
                m.set_cell(j + sx, i + sy, "@")
            else:
                m.set_cell(j + sx, i + sy, "#")

    return bfs_bots(m, m.find_cell("@"))


def main() -> None:
    test_cases()

    with open("input_18.txt", "r") as f:
        input_txt = f.read().strip()

    # print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
