from collections import deque
from typing import Any, Optional


def deal_into_new_stack(deck: deque[int]) -> deque[int]:
    new_deck: deque[int] = deque()
    while deck:
        new_deck.appendleft(deck.popleft())
    return new_deck


def cut(deck: deque[int], x: int) -> deque[int]:
    deck.rotate(-x)
    return deck


def deal_with_increment(deck: deque[int], x: int) -> deque[int]:
    new_deck = deck.copy()
    pos = 0
    n = len(deck)
    for c in deck:
        new_deck[pos] = c
        pos += x
        pos %= n
    return new_deck


def multiple_deals(deck: deque[int], deals: list[str]) -> deque[int]:
    for deal in deals:
        s = deal.split(" ")
        try:
            x = int(s[-1])
            name = " ".join(s[:-1])
        except ValueError:
            name = " ".join(s)
            x = 0

        if name == "deal into new stack":
            deck = deal_into_new_stack(deck)
        elif name == "cut":
            deck = cut(deck, x)
        elif name == "deal with increment":
            deck = deal_with_increment(deck, x)
        else:
            raise ValueError(f"Shuffle `{name}` not recognized")

    return deck


def deque_to_str(d: deque[Any]) -> str:
    return " ".join([str(x) for x in d])


def test_cases() -> None:
    deals = [
        "deal with increment 7",
        "deal into new stack",
        "deal into new stack",
    ]
    deck = deque(range(10))
    assert deque_to_str(multiple_deals(deck, deals)) == "0 3 6 9 2 5 8 1 4 7"

    deals = [
        "cut 6",
        "deal with increment 7",
        "deal into new stack",
    ]
    deck = deque(range(10))
    assert deque_to_str(multiple_deals(deck, deals)) == "3 0 7 4 1 8 5 2 9 6"

    deals = [
        "deal with increment 7",
        "deal with increment 9",
        "cut -2",
    ]
    deck = deque(range(10))
    assert deque_to_str(multiple_deals(deck, deals)) == "6 3 0 7 4 1 8 5 2 9"

    deals = [
        "deal into new stack",
        "cut -2",
        "deal with increment 7",
        "cut 8",
        "cut -4",
        "deal with increment 7",
        "cut 3",
        "deal with increment 9",
        "deal with increment 3",
        "cut -1",
    ]
    deck = deque(range(10))
    assert deque_to_str(multiple_deals(deck, deals)) == "9 2 5 8 1 4 7 0 3 6"


def part1(input_txt: str) -> int:
    deck = deque(range(10007))
    deals = input_txt.splitlines()
    deck = multiple_deals(deck, deals)
    return deck.index(2019)


def part2(input_txt: str) -> int:
    deck_size = 119315717514047
    times = 101741582076661
    pos = 2020
    a, b = 1, 0
    for deal in input_txt.splitlines():
        s = deal.split(" ")
        try:
            x = int(s[-1])
            name = " ".join(s[:-1])
        except ValueError:
            name = " ".join(s)
            x = 0
        if name == "deal into new stack":
            a = (deck_size - a) % deck_size
            b = (deck_size - b - 1) % deck_size
        elif name == "cut":
            b = (b - x) % deck_size
        elif name == "deal with increment":
            a = (x * a) % deck_size
            b = (x * b) % deck_size
        else:
            raise ValueError(f"Shuffle `{name}` not recognized")

    r = (b * pow(1 - a, deck_size - 2, deck_size)) % deck_size
    return ((pos - r) * pow(a, times *
                            (deck_size - 2), deck_size) + r) % deck_size


def main() -> None:
    test_cases()

    with open("input_22.txt", "r") as f:
        input_txt = f.read().strip()

    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
