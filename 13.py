from dataclasses import dataclass
from math import inf
from typing import Dict, List, Optional

from intcode_computer import IntcodeComputer


@dataclass(frozen=True, eq=True)
class Point:
    x: int
    y: int


def generate_tileset(
    output_sequence: List[int],
    tileset: Optional[Dict[int,
                           List[Point]]] = None) -> Dict[int, List[Point]]:
    if tileset is None:
        tileset = {}
    tiles_xpos = output_sequence[::3]
    tiles_ypos = output_sequence[1::3]
    tiles_ids = output_sequence[2::3]

    for (x, y, tile_id) in zip(tiles_xpos, tiles_ypos, tiles_ids):
        if not 0 <= tile_id <= 4 and x >= 0 and y >= 0:
            raise ValueError("Unrecognized ID %d at (%d, %d)" %
                             (tile_id, x, y))
        if tile_id not in tileset:
            tileset[tile_id] = []
        tileset[tile_id].append(Point(x, y))

    return tileset


def part1(input_txt: str) -> int:
    program = [int(x) for x in input_txt.split(",")]
    computer = IntcodeComputer(program)
    output_sequence: List[int] = []

    while computer.run() != 0:
        output_sequence.append(computer.out)

    tileset = generate_tileset(output_sequence)

    return len(tileset[2])


def part2(input_txt: str) -> int:
    program = [int(x) for x in input_txt.split(",")]
    program[0] = 2
    computer = IntcodeComputer(program)
    output_sequence: List[int] = []
    tileset: Optional[Dict[int, List[Point]]] = None

    exec_code = computer.run()
    while exec_code != 0:
        if exec_code == 1:
            tileset = generate_tileset(output_sequence, tileset)
            output_sequence.clear()
            paddle_pos = tileset[3][-1]
            ball_pos = tileset[4][-1]

            if paddle_pos.x < ball_pos.x:
                input_code = 1
            elif paddle_pos.x > ball_pos.x:
                input_code = -1
            else:
                input_code = 0
            computer.add_inputs(input_code)
        elif exec_code == 2:
            output_sequence.append(computer.out)

        exec_code = computer.run()

    return computer.out


def test_cases() -> None:
    output_sequence: List[int] = [1, 2, 3, 6, 5, 4]
    assert generate_tileset(output_sequence)[3][0] == Point(1, 2)


def main() -> None:
    test_cases()

    with open("input_13.txt", "r") as f:
        input_txt: str = f.read().strip("\r\n")

    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
