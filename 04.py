from collections import Counter


def test_password1(password: str) -> bool:
    if password != "".join(sorted(password)):
        return False

    if max(Counter(password).values()) == 1:
        return False

    return True


def test_password2(password: str) -> bool:
    if password != "".join(sorted(password)):
        return False

    if 2 not in Counter(password).values():
        return False

    return True


def part1(min_range: int, max_range: int) -> int:
    count = 0
    for password in range(min_range, max_range + 1):
        count += test_password1(str(password))

    return count


def part2(min_range: int, max_range: int) -> int:
    count = 0
    for password in range(min_range, max_range + 1):
        count += test_password2(str(password))

    return count


if __name__ == "__main__":
    input_str = "265275-781584"
    pass_range = [int(x) for x in input_str.split("-")]

    # Test cases
    assert test_password1("111111")
    assert not test_password1("223450")
    assert not test_password1("123789")

    assert test_password2("112233")
    assert not test_password2("123444")
    assert test_password2("111122")

    print(part1(*pass_range))
    print(part2(*pass_range))
