from collections import Counter
from dataclasses import dataclass
from math import inf
from typing import Optional

from intcode_computer import IntcodeComputer

# from visualization_15 import Map


@dataclass(frozen=True)
class Position:
    x: int
    y: int


DIRECTIONS = {1: "n", 2: "s", 3: "w", 4: "e"}


def invert_direction(direction: int) -> int:
    if direction == 1:
        return 2
    elif direction == 2:
        return 1
    elif direction == 3:
        return 4
    else:
        return 3


def direction_from_to(pos_from: Position, pos_to: Position) -> int:
    if pos_from.x == pos_to.x:
        if pos_from.y > pos_to.y:
            return 1
        else:
            return 2
    else:
        if pos_from.x > pos_to.x:
            return 3
        else:
            return 4


def move(position: Position, direction: int) -> Position:
    if direction == 1:
        return Position(position.x, position.y - 1)
    elif direction == 2:
        return Position(position.x, position.y + 1)
    elif direction == 3:
        return Position(position.x - 1, position.y)
    else:
        return Position(position.x + 1, position.y)


def move_back(position: Position, direction: int) -> Position:
    return move(position, invert_direction(direction))


def get_empty_adjacent_positions(world: dict[Position, str],
                                 position: Position) -> list[Position]:
    adjacents = [
        Position(position.x + 1, position.y),
        Position(position.x - 1, position.y),
        Position(position.x, position.y + 1),
        Position(position.x, position.y - 1),
    ]

    empty_adjacents: list[Position] = []
    for adjacent in adjacents:
        if world[adjacent] == ".":
            empty_adjacents.append(adjacent)

    return empty_adjacents


def draw_world(world: dict[Position, str],
               current_position: Optional[Position] = None) -> None:
    if current_position is None:
        current_position = Position(0, 0)
    min_x = 0
    max_x = 0
    min_y = 0
    max_y = 0
    for position in world:
        min_x = min(position.x, min_x)
        min_y = min(position.y, min_y)
        max_x = max(position.x, max_x)
        max_y = max(position.y, max_y)

    world_str = [[" " for _ in range(max_x - min_x + 1)]
                 for _ in range(max_y - min_y + 1)]

    for position in world:
        world_str[position.y - min_y][position.x - min_x] = world[position]
    world_str[current_position.y - min_y][current_position.x - min_x] = "D"

    for row in world_str:
        for col in row:
            print(col, end="")
        print()


def part1(input_txt: str, world: dict[Position, str]) -> int:
    program = [int(i) for i in input_txt.split(',')]
    c = IntcodeComputer(program)

    moves: list[Position] = []
    position = Position(0, 0)

    min_moves: float = inf

    # vis = Map()

    while True:
        no_moves_left: bool = True
        for direction in range(1, 5):
            # update position
            position = move(position, direction)

            # check if position was already accesed
            if position in world:
                position = move_back(position, direction)
                continue

            # execute program with input
            c.add_inputs(direction)
            exec_code: int = c.run()

            if exec_code == 2:
                if c.out == 0:
                    world[position] = "#"
                    # vis.add_wall((position.x, position.y))
                    position = move_back(position, direction)
                elif c.out == 1:
                    world[position] = "."
                    moves.append(position)
                    # vis.add_visited((position.x, position.y))
                    no_moves_left = False
                    break
                elif c.out == 2:
                    world[position] = "x"
                    min_moves = min(min_moves, len(moves) + 1)
                    # vis.add_destination((position.x, position.y))
                    position = move_back(position, direction)
                    c.add_inputs(invert_direction(direction))
                    c.run()
                    no_moves_left = False
                    break

            # draw_world(world, position)
            # print()

        if no_moves_left:
            if len(moves) <= 1:
                break
            last_position = moves.pop()

            backtrack_direction = direction_from_to(last_position, moves[-1])
            position = move(position, backtrack_direction)
            c.add_inputs(backtrack_direction)
            c.run()

        # vis.update_position((position.x, position.y))
        # if not vis.update():
        #     break

    return int(min_moves)


def part2(input_txt: str, world: dict[Position, str]) -> int:
    minutes: int = 0
    empty_cells: int = Counter(world.values())["."]
    for position in world:
        if world[position] == "x":
            starting_position: Position = position
            break
    oxygen_positions: list[Position] = [starting_position]
    world[starting_position] = "O"

    while empty_cells > 0:
        minutes += 1
        new_oxygen_positions: list[Position] = []
        for position in oxygen_positions:
            for adjacent in get_empty_adjacent_positions(world, position):
                world[adjacent] = "O"
                new_oxygen_positions.append(adjacent)
                empty_cells -= 1

        oxygen_positions.extend(new_oxygen_positions)

    return minutes


def main() -> None:
    with open("input_15.txt", "r") as f:
        input_txt = f.read().strip()

    world: dict[Position, str] = {}
    print(part1(input_txt, world))
    print(part2(input_txt, world))


if __name__ == "__main__":
    main()
