from __future__ import annotations

from math import lcm
from typing import List, Tuple


class Moon:
    def __init__(self, x: int, y: int, z: int):
        self.velocity: List[int] = [0, 0, 0]
        self.position: List[int] = [x, y, z]

        self.starting_position = self.position.copy()

    def __repr__(self) -> str:
        return (f"pos=<x={self.position[0]:3d}, y={self.position[1]:3d}, "
                f"z={self.position[2]:3d}>, vel=<x={self.velocity[0]:3d}, "
                f"y={self.velocity[1]:3d}, z={self.velocity[2]:3d}>")

    def restart(self) -> None:
        self.position = self.starting_position.copy()
        self.velocity = [0, 0, 0]

    def update_velocities(self, moons: List[Moon]) -> None:
        for moon in moons:
            for i in range(3):
                if self.position[i] < moon.position[i]:
                    self.velocity[i] += 1
                    moon.velocity[i] -= 1
                elif self.position[i] > moon.position[i]:
                    self.velocity[i] -= 1
                    moon.velocity[i] += 1

    def update_positions(self) -> None:
        for i in range(3):
            self.position[i] += self.velocity[i]

    def energy(self) -> int:
        pot: int = sum(map(abs, self.position))
        kin: int = sum(map(abs, self.velocity))

        return pot * kin


def compare_moons_with_starting_position(moons: List[Moon], ix: int) -> bool:
    result: bool = True
    for moon in moons:
        result &= moon.velocity[ix] == 0 and moon.position[
            ix] == moon.starting_position[ix]

    return result


def simulate(moons: List[Moon], steps: int) -> int:
    for _ in range(steps):
        for i, moon in enumerate(moons):
            moon.update_velocities(moons[i + 1:])
        for moon in moons:
            moon.update_positions()

    total_energy: int = 0
    for moon in moons:
        total_energy += moon.energy()

    return total_energy


def find_repeated_state(moons: List[Moon]) -> int:
    steps: int = 0
    cycles: List[int] = [0, 0, 0]
    done: int = 0
    while True:
        steps += 1
        for i, moon in enumerate(moons):
            moon.update_velocities(moons[i + 1:])
        for moon in moons:
            moon.update_positions()

        for i in range(3):
            if cycles[i] != 0:
                continue
            if compare_moons_with_starting_position(moons, i):
                cycles[i] = steps
                done += 1

        if done == 3:
            break

    return int(lcm(*cycles))


def test_cases() -> None:
    moons: List[Moon] = [
        Moon(-1, 0, 2),
        Moon(2, -10, -7),
        Moon(4, -8, 8),
        Moon(3, 5, -1)
    ]

    assert simulate(moons, 10) == 179
    for moon in moons:
        moon.restart()
    assert find_repeated_state(moons) == 2772

    moons = [Moon(-8, -10, 0), Moon(5, 5, 10), Moon(2, -7, 3), Moon(9, -8, -3)]

    assert simulate(moons, 100) == 1940
    for moon in moons:
        moon.restart()
    assert find_repeated_state(moons) == 4686774924


def part1(moons: List[Moon]) -> int:
    return simulate(moons, 1000)


def part2(moons: List[Moon]) -> int:
    for moon in moons:
        moon.restart()

    return find_repeated_state(moons)


def main() -> None:
    test_cases()

    with open("input_12.txt", "r") as f:
        input_txt = f.read().splitlines()

    moons: List[Moon] = []
    for moon in input_txt:
        sep1 = moon.find('=')
        sep2 = moon.find(',')
        position: List[int] = []
        while sep1 != -1:
            position.append(int(moon[sep1 + 1:sep2]))
            sep1 = moon.find('=', sep1 + 1)
            sep2 = moon.find(',', sep1 + 1)

        moons.append(Moon(*position))

    print(part1(moons))
    print(part2(moons))


if __name__ == "__main__":
    main()
