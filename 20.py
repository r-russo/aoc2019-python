from collections import deque

Point = tuple[int, int]


class Maze:
    def __init__(self, input_txt: str):
        self.map = [list(line) for line in input_txt.splitlines()]
        self.width = len(self.map[0])
        self.height = len(self.map)
        assert not list(filter(lambda x: len(x) != len(self.map[0]), self.map))

        self.starting_position = self.find_tag("AA")[0]
        self.goal = self.find_tag("ZZ")[0]
        self.portals = self.find_portals()

    def on_outer_edge(self, x: int, y: int) -> bool:
        return x == 2 or x == self.width - 3 or y == 2 or y == self.height - 3

    def get_cell(self, x: int, y: int) -> str:
        if 0 <= x < self.width and 0 <= y < self.height:
            return self.map[y][x]
        return " "

    def get_neighbour_cells(self, x: int, y: int) -> list[str]:
        pass

    def read_tag(self, x: int, y: int) -> str:
        cell = self.get_cell(x, y)
        if not cell.isupper():
            return ""

        tag = cell + self.get_cell(x + 1, y)
        if tag.isupper() and tag.isalpha():
            return tag

        tag = cell + self.get_cell(x, y + 1)
        if tag.isupper() and tag.isalpha():
            return tag

        tag = self.get_cell(x - 1, y) + cell
        if tag.isupper() and tag.isalpha():
            return tag

        tag = self.get_cell(x, y - 1) + cell
        if tag.isupper() and tag.isalpha():
            return tag

        return ""

    def find_tag(self, tag: str) -> list[Point]:
        matches: list[Point] = []
        for x in range(2, self.width - 2):
            for y in range(2, self.height - 2):
                if self.get_cell(x, y) != ".":
                    continue

                for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
                    if self.read_tag(x + dx, y + dy) == tag:
                        matches.append((x, y))

        return matches

    def get_list_tags(self) -> list[str]:
        tags: list[str] = []
        for x in range(2, self.width - 2):
            for y in range(2, self.height - 2):
                if self.get_cell(x, y) != ".":
                    continue

                for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
                    tag = self.read_tag(x + dx, y + dy)
                    if tag != "" and tag not in tags:
                        tags.append(tag)

        return tags

    def find_portals(self) -> dict[str, list[Point]]:
        portals: dict[str, list[Point]] = {}

        for tag in self.get_list_tags():
            if tag in ["AA", "ZZ"]:
                continue

            portals[tag] = self.find_tag(tag)

        return portals

    def get_traversable_neighbours(self, x: int,
                                   y: int) -> list[tuple[int, int, str]]:
        neighbours: list[tuple[int, int, str]] = []
        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            nx, ny = x + dx, y + dy
            cell = self.get_cell(nx, ny)
            tag = self.read_tag(nx, ny)
            if cell == ".":
                neighbours.append((nx, ny, tag))
            elif tag in self.portals:
                portal = self.portals[tag]
                if portal[0] == (x, y):
                    neighbours.append((*portal[1], tag))
                else:
                    neighbours.append((*portal[0], tag))
            elif tag == "ZZ":
                neighbours.append((nx, ny, tag))

        return neighbours


def bfs(maze: Maze) -> int:
    q: deque[tuple[int, int, int]] = deque()
    visited: set[tuple[int, int]] = set()

    start = maze.starting_position
    q.appendleft((*start, 0))
    visited.add(start)
    while deque:
        x, y, steps = q.pop()
        if (x, y) == maze.goal:
            return steps
        for nx, ny, _ in maze.get_traversable_neighbours(x, y):
            if (nx, ny) not in visited:
                visited.add((nx, ny))
                q.appendleft((nx, ny, steps + 1))


def bfs_levels(maze: Maze) -> int:
    q: deque[tuple[int, int, int, int]] = deque()
    visited: set[tuple[int, int, int]] = set()

    start = maze.starting_position
    q.appendleft((*start, 0, 0))
    visited.add((*start, 0))
    while deque:
        x, y, steps, level = q.pop()
        if (x, y, level) == (*maze.goal, 0):
            return steps
        for nx, ny, tag in maze.get_traversable_neighbours(x, y):
            new_level = level
            if tag != "":
                if maze.on_outer_edge(x, y):
                    if level == 0 or tag in {"AA", "ZZ"}:
                        continue
                    else:
                        new_level -= 1
                else:
                    new_level += 1

            if (nx, ny, new_level) not in visited:
                visited.add((nx, ny, new_level))
                q.appendleft((nx, ny, steps + 1, new_level))


def test_cases() -> None:
    input_txt = """         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       """
    maze = Maze(input_txt)
    assert bfs(maze) == 23

    input_txt = """                   A               
                   A               
  #################.#############  
  #.#...#...................#.#.#  
  #.#.#.###.###.###.#########.#.#  
  #.#.#.......#...#.....#.#.#...#  
  #.#########.###.#####.#.#.###.#  
  #.............#.#.....#.......#  
  ###.###########.###.#####.#.#.#  
  #.....#        A   C    #.#.#.#  
  #######        S   P    #####.#  
  #.#...#                 #......VT
  #.#.#.#                 #.#####  
  #...#.#               YN....#.#  
  #.###.#                 #####.#  
DI....#.#                 #.....#  
  #####.#                 #.###.#  
ZZ......#               QG....#..AS
  ###.###                 #######  
JO..#.#.#                 #.....#  
  #.#.#.#                 ###.#.#  
  #...#..DI             BU....#..LF
  #####.#                 #.#####  
YN......#               VT..#....QG
  #.###.#                 #.###.#  
  #.#...#                 #.....#  
  ###.###    J L     J    #.#.###  
  #.....#    O F     P    #.#...#  
  #.###.#####.#.#####.#####.###.#  
  #...#.#.#...#.....#.....#.#...#  
  #.#####.###.###.#.#.#########.#  
  #...#.#.....#...#.#.#.#.....#.#  
  #.###.#####.###.###.#.#.#######  
  #.#.........#...#.............#  
  #########.###.###.#############  
           B   J   C               
           U   P   P               """
    maze = Maze(input_txt)
    assert bfs(maze) == 58

    input_txt = """             Z L X W       C                 
             Z P Q B       K                 
  ###########.#.#.#.#######.###############  
  #...#.......#.#.......#.#.......#.#.#...#  
  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  
  #.#...#.#.#...#.#.#...#...#...#.#.......#  
  #.###.#######.###.###.#.###.###.#.#######  
  #...#.......#.#...#...#.............#...#  
  #.#########.#######.#.#######.#######.###  
  #...#.#    F       R I       Z    #.#.#.#  
  #.###.#    D       E C       H    #.#.#.#  
  #.#...#                           #...#.#  
  #.###.#                           #.###.#  
  #.#....OA                       WB..#.#..ZH
  #.###.#                           #.#.#.#  
CJ......#                           #.....#  
  #######                           #######  
  #.#....CK                         #......IC
  #.###.#                           #.###.#  
  #.....#                           #...#.#  
  ###.###                           #.#.#.#  
XF....#.#                         RF..#.#.#  
  #####.#                           #######  
  #......CJ                       NM..#...#  
  ###.#.#                           #.###.#  
RE....#.#                           #......RF
  ###.###        X   X       L      #.#.#.#  
  #.....#        F   Q       P      #.#.#.#  
  ###.###########.###.#######.#########.###  
  #.....#...#.....#.......#...#.....#.#...#  
  #####.#.###.#######.#######.###.###.#.#.#  
  #.......#.......#.#.#.#.#...#...#...#.#.#  
  #####.###.#####.#.#.#.#.###.###.#.###.###  
  #.......#.....#.#...#...............#...#  
  #############.#.#.###.###################  
               A O F   N                     
               A A D   M                     """
    maze = Maze(input_txt)
    assert bfs_levels(maze) == 396


def part1(input_txt: str) -> int:
    return bfs(Maze(input_txt))


def part2(input_txt: str) -> int:
    return bfs_levels(Maze(input_txt))


def main() -> None:
    with open("input_20.txt", "r") as f:
        input_txt = f.read().strip("\n")

    test_cases()
    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
