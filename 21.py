from intcode_computer import IntcodeComputer


def convert_ascii_to_input(command: str) -> list[int]:
    command = command.strip() + "\n"
    return [ord(c) for c in command]


def simulate(program: list[int], commands: list[str]) -> int:
    c = IntcodeComputer(program)
    for command in commands:
        c.add_inputs(*convert_ascii_to_input(command))
    exec_code: int = c.run()
    output = ""
    while exec_code != 0:
        if exec_code == 2:
            if c.out not in range(0x110000):
                return c.out
            output += chr(c.out)
        exec_code = c.run()

    print(output)
    return -1


def part1(program: list[int]) -> int:
    commands = [
        "NOT A J",
        "NOT B T",
        "OR T J",
        "NOT C T",
        "OR T J",
        "AND D J",
        "WALK",
    ]
    return simulate(program, commands)


def part2(program: list[int]) -> int:
    commands = [
        "NOT A J",
        "NOT B T",
        "OR T J",
        "NOT C T",
        "AND H T",
        "OR T J",
        "AND D J",
        "RUN",
    ]
    return simulate(program, commands)


def main() -> None:
    with open("input_21.txt", "r") as f:
        input_txt = f.read().rstrip()

    program = [int(x) for x in input_txt.split(",")]
    print(part1(program))
    print(part2(program))


if __name__ == "__main__":
    main()
