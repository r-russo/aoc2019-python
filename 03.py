from typing import List


class Coordinate:
    def __init__(self, x=0, y=0, valid=True):
        self.x = x
        self.y = y
        self.valid = valid

    def get(self):
        return (self.x, self.y)

    def distance(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y)

    def __repr__(self):
        return f"Coordinate({self.x}, {self.y})"

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y


def get_coordinates(path: str) -> List[Coordinate]:
    coordinates = [Coordinate(0, 0)]
    for movement in path.split(","):
        direction = movement[0]
        delta = int(movement[1:])
        x0, y0 = coordinates[-1].get()
        if direction == "R":
            coordinates.append(Coordinate(x0 + delta, y0))
        elif direction == "L":
            coordinates.append(Coordinate(x0 - delta, y0))
        elif direction == "U":
            coordinates.append(Coordinate(x0, y0 - delta))
        elif direction == "D":
            coordinates.append(Coordinate(x0, y0 + delta))

    return coordinates


def find_crossing(p: List[Coordinate], q: List[Coordinate]) -> Coordinate:
    if len(p) != 2 and len(q) != 2:
        return Coordinate(valid=False)

    if p[0].x == p[1].x:
        if q[0].x <= p[0].x <= q[1].x or q[1].x <= p[0].x <= q[0].x:
            if p[0].y <= q[0].y <= p[1].y or p[1].y <= q[0].y <= p[0].y:
                return Coordinate(p[0].x, q[0].y)
    else:
        if p[0].x <= q[0].x <= p[1].x or p[1].x <= q[0].x <= p[0].x:
            if q[0].y <= p[0].y <= q[1].y or q[1].y <= p[0].y <= q[0].y:
                return Coordinate(q[0].x, p[0].y)

    return Coordinate(valid=False)


def part1(path1: str, path2: str) -> int:
    coordinates1 = get_coordinates(path1)
    coordinates2 = get_coordinates(path2)
    crossings = []
    for p1, p2 in zip(coordinates1[1:], coordinates1[2:]):
        for q1, q2 in zip(coordinates2[1:], coordinates2[2:]):
            c = find_crossing([p1, p2], [q1, q2])
            if c.valid:
                crossings.append(c)

    return min(map(lambda c: abs(c.x) + abs(c.y), crossings))


def part2(path1: str, path2: str) -> int:
    coordinates1 = get_coordinates(path1)
    coordinates2 = get_coordinates(path2)
    steps: List[int] = []
    steps_p = 0
    last_p = coordinates1[0]
    for p1, p2 in zip(coordinates1[1:], coordinates1[2:]):
        steps_p += p1.distance(last_p)
        last_p = p1
        steps_q = 0
        last_q = coordinates2[0]
        for q1, q2 in zip(coordinates2[1:], coordinates2[2:]):
            steps_q += q1.distance(last_q)
            last_q = q1
            c = find_crossing([p1, p2], [q1, q2])
            if c.valid:
                steps.append(steps_p + steps_q + c.distance(last_p) +
                             c.distance(last_q))

    return min(steps)


if __name__ == "__main__":
    with open("input_03.txt", "r") as f:
        paths = f.read().splitlines()

    # Test cases
    assert part1("R8,U5,L5,D3", "U7,R6,D4,L4") == 6
    assert part1("R75,D30,R83,U83,L12,D49,R71,U7,L72",
                 "U62,R66,U55,R34,D71,R55,D58,R83") == 159
    assert part1("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
                 "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7") == 135
    assert part2("R8,U5,L5,D3", "U7,R6,D4,L4") == 30
    assert part2("R75,D30,R83,U83,L12,D49,R71,U7,L72",
                 "U62,R66,U55,R34,D71,R55,D58,R83") == 610
    assert part2("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
                 "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7") == 410

    print(part1(paths[0], paths[1]))
    print(part2(paths[0], paths[1]))
