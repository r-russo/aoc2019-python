from typing import List

from intcode_computer import IntcodeComputer


def part1(program: List[int]) -> int:
    program[1] = 12
    program[2] = 2
    computer = IntcodeComputer(program)
    computer.run()
    return computer.program[0]


def part2(program: List[int]) -> int:
    for i1 in range(0, 100):
        for i2 in range(0, 100):
            new_program = program.copy()
            new_program[1] = i1
            new_program[2] = i2

            if execute_program(new_program)[0] == 19690720:
                return 100 * i1 + i2

    return 0


if __name__ == "__main__":
    with open("input_02.txt", "r") as f:
        input_str = f.read()

    program = [int(x) for x in input_str.strip("\n").split(",")]

    # Test cases
    assert ",".join([str(x) for x in execute_program([1, 0, 0, 0, 99])
                     ]) == "2,0,0,0,99"
    assert ",".join([str(x) for x in execute_program([2, 3, 0, 3, 99])
                     ]) == "2,3,0,6,99"
    assert ",".join([str(x) for x in execute_program([2, 4, 4, 5, 99, 0])
                     ]) == "2,4,4,5,99,9801"
    assert ",".join([
        str(x) for x in execute_program([1, 1, 1, 4, 99, 5, 6, 0, 99])
    ]) == "30,1,1,4,2,5,6,0,99"

    print(part1(program.copy()))
    print(part2(program.copy()))
