from typing import List

from intcode_computer import IntcodeComputer


def execute_program(program: List[int], input_value: int) -> List[int]:
    pc: int = 0
    while program[pc] != 99:
        opcode: str = f"{program[pc]:05d}"
        instruction: int = int(opcode[-2:])
        modes: List[int] = [int(x) for x in opcode[:-2]][::-1]

        # print(opcode, instruction, modes)
        num_params: int = 0
        if instruction < 3:
            num_params = 2
        elif instruction in [5, 6]:
            num_params = 1
        elif instruction in [7, 8]:
            num_params = 2

        params: List[int] = [
            program[pc + x + 1] if modes[x] == 1 else program[program[pc + x +
                                                                      1]]
            for x in range(num_params)
        ]

        dest: int = program[pc + 1 + num_params]
        branch: bool = False

        if instruction == 1:
            program[dest] = params[0] + params[1]
        elif instruction == 2:
            program[dest] = params[0] * params[1]
        elif instruction == 3:
            program[dest] = input_value
        elif instruction == 4:
            print(program[dest] if modes[0] == 0 else dest)
        elif instruction == 5:
            if params[0] != 0:
                branch = True
        elif instruction == 6:
            if params[0] == 0:
                branch = True
        elif instruction == 7:
            program[dest] = 1 if params[0] < params[1] else 0
        elif instruction == 8:
            program[dest] = 1 if params[0] == params[1] else 0

        if not branch:
            pc += 2 + num_params
        else:
            pc = program[dest] if modes[num_params] == 0 else dest
            print(pc)

    return program


if __name__ == "__main__":
    with open("input_05.txt", "r") as f:
        input_str = f.read()

    program = [int(x) for x in input_str.strip("\n").split(",")]

    # Test cases
    print("Test cases:")
    execute_program([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 8)  # Equal 8
    execute_program([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 7)  # Less than 8
    execute_program([3, 3, 1108, -1, 8, 3, 4, 3, 99], 8)  # Equal 8
    execute_program([3, 3, 1107, -1, 8, 3, 4, 3, 99], 7)  # Less than 8
    print("Part 1")
    execute_program(program.copy(), 1)
    print("Part 2")
    execute_program(program.copy(), 5)
