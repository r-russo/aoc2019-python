from collections import OrderedDict
from dataclasses import dataclass
from math import ceil, inf
from typing import Any


def get_reactions(input_txt: str) -> dict[str, Any]:
    reactions = {}
    for line in input_txt.split('\n'):
        materials, result = line.split(" => ")
        result_quantity, result_name = result.split(" ")
        reaction = {"materials": {}, "output": int(result_quantity)}

        for material in materials.split(", "):
            material_quantity, material_name = material.split(" ")
            reaction["materials"][material_name] = int(material_quantity)

            reactions[result_name] = reaction

    return reactions


def get_number_of_ores_rec(reactions: dict[str, Any], material: str,
                           quantity: int, leftovers: dict[str, int]) -> int:
    current_formula = reactions[material]
    output = int(current_formula["output"])
    if material in leftovers and leftovers[material] > 0:
        quantity -= leftovers[material]
        leftovers[material] = 0
    needed_materials = ceil(quantity / output)
    if material in leftovers:
        leftovers[material] += needed_materials * output - quantity
    else:
        leftovers[material] = needed_materials * output - quantity

    if "ORE" in current_formula["materials"]:
        return int(current_formula["materials"]["ORE"]) * needed_materials

    total_quantity: int = 0
    for new_material in current_formula["materials"]:
        total_quantity += get_number_of_ores_rec(
            reactions, new_material,
            current_formula["materials"][new_material] * needed_materials,
            leftovers)

    return total_quantity


def get_number_of_ores(input_txt: str) -> int:
    reactions = get_reactions(input_txt)
    leftovers: dict[str, int] = {}

    return get_number_of_ores_rec(reactions, "FUEL", 1, leftovers)


def maximum_amount_fuel(input_txt: str, ores: int = 1000000000000) -> int:
    reactions = get_reactions(input_txt)
    leftovers: dict[str, int] = {}

    needed_ores = get_number_of_ores_rec(reactions, "FUEL", 1000, leftovers)
    leftovers = {k: v for k, v in leftovers.items() if v}

    lower_bound = ores * 1000 // needed_ores
    upper_bound = ores * 1000 * 2 // needed_ores

    while lower_bound <= upper_bound:
        fuel = (upper_bound + lower_bound) // 2
        needed_ores = get_number_of_ores_rec(reactions, "FUEL", fuel,
                                             leftovers)
        if needed_ores <= ores:
            lower_bound = fuel + 1
        else:
            upper_bound = fuel - 1

    needed_ores = get_number_of_ores_rec(reactions, "FUEL", fuel, {})
    while needed_ores > ores:
        fuel -= 1
        needed_ores = get_number_of_ores_rec(reactions, "FUEL", fuel, {})

    return fuel


def test_cases() -> None:
    input_txt = """9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL"""

    assert get_number_of_ores(input_txt) == 165

    input_txt = """157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT"""
    assert get_number_of_ores(input_txt) == 13312
    assert maximum_amount_fuel(input_txt) == 82892753

    input_txt = """2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF"""
    assert get_number_of_ores(input_txt) == 180697
    assert maximum_amount_fuel(input_txt) == 5586022

    input_txt = """171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX"""
    assert get_number_of_ores(input_txt) == 2210736
    assert maximum_amount_fuel(input_txt) == 460664


def part1(input_txt: str) -> int:
    return get_number_of_ores(input_txt)


def part2(input_txt: str) -> int:
    return maximum_amount_fuel(input_txt)


def main() -> None:
    test_cases()

    with open("input_14.txt", "r") as f:
        input_txt = f.read().strip()

    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
