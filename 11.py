from enum import Enum
from typing import List, Tuple

from intcode_computer import IntcodeComputer


class Direction(Enum):
    N = 0
    W = 1
    S = 2
    E = 3


def get_char(d: Direction):
    if d == Direction.N:
        return "^"
    elif d == Direction.W:
        return "<"
    elif d == Direction.S:
        return "v"
    else:
        return ">"


class PaintingRobot:
    def __init__(self, width: int = 100, height: int = 100):
        self.map: List[List[str]] = [["." for _ in range(width)]
                                     for _ in range(height)]
        self.direction: Direction = Direction.N
        self.next_color: str = '#'
        self.painted_cells: List[Tuple[int, int]] = []
        self.movements: int = 0
        self.x = width // 2
        self.y = height // 2

    def draw(self) -> None:
        for y, row in enumerate(self.map):
            for x, col in enumerate(row):
                if x == self.x and y == self.y:
                    print(get_char(self.direction), end='')
                else:
                    print(col, end='')

            print()

    def get_current_cell(self) -> int:
        return 0 if self.map[self.y][self.x] == '.' else 1

    def input(self, color: int, direction: int) -> None:
        self.next_color = '#' if color else '.'
        self.turn_right() if direction else self.turn_left()

    def advance(self) -> None:
        self.movements += 1
        self.map[self.y][self.x] = self.next_color
        if (self.x, self.y) not in self.painted_cells:
            self.painted_cells.append((self.x, self.y))
        if self.direction == Direction.N:
            self.y -= 1
        elif self.direction == Direction.W:
            self.x -= 1
        elif self.direction == Direction.S:
            self.y += 1
        else:
            self.x += 1

        if self.x < 0 or self.x >= len(self.map[0]) or \
                self.y < 0 or self.y >= len(self.map):
            raise ValueError("Moving outside of map")

    def turn_left(self) -> None:
        if self.direction == Direction.N:
            self.direction = Direction.W
        elif self.direction == Direction.W:
            self.direction = Direction.S
        elif self.direction == Direction.S:
            self.direction = Direction.E
        else:
            self.direction = Direction.N

    def turn_right(self) -> None:
        if self.direction == Direction.N:
            self.direction = Direction.E
        elif self.direction == Direction.E:
            self.direction = Direction.S
        elif self.direction == Direction.S:
            self.direction = Direction.W
        else:
            self.direction = Direction.N

    def count_painted_cells(self) -> int:
        return len(self.painted_cells)


def part1(p: PaintingRobot, input_program: str) -> int:
    computer = IntcodeComputer([int(x) for x in input_program.split(',')])
    computer.add_inputs(p.get_current_cell())
    outputs: List[int] = []
    while computer.run() != 0:
        outputs.append(computer.out)
        if len(outputs) == 2:
            p.input(outputs[0], outputs[1])
            p.advance()
            computer.add_inputs(p.get_current_cell())
            outputs = []

    return p.count_painted_cells()


def test_cases() -> None:
    p = PaintingRobot()
    p.input(1, 0)
    p.advance()
    p.input(0, 0)
    p.advance()
    p.input(1, 0)
    p.advance()
    p.input(1, 0)
    p.advance()
    p.input(0, 1)
    p.advance()
    p.input(1, 0)
    p.advance()
    p.input(1, 0)
    p.advance()

    assert p.count_painted_cells() == 6


def main() -> None:
    test_cases()

    with open("input_11.txt", 'r') as f:
        program = f.read().strip('\n\r')

    p = PaintingRobot()
    print(part1(p, program))
    p = PaintingRobot()
    p.map[p.y][p.x] = '#'
    part1(p, program)
    p.draw()


if __name__ == "__main__":
    main()
