from math import inf
from typing import List


def convert_str_to_layers(input_str: str, rows: int,
                          cols: int) -> List[List[str]]:
    n_layers = len(input_str) // (rows * cols)
    return [[
        input_str[(rows * layer + row) * cols:(rows * layer + row + 1) * cols]
        for row in range(rows)
    ] for layer in range(n_layers)]


def part1(input_str: str, rows: int, cols: int) -> int:
    image = convert_str_to_layers(input_str, rows, cols)
    min_zeros: float = inf
    min_ix: int = 0
    for i, layer in enumerate(image):
        flat_layer = [item for row in layer for item in row]
        count_zeros: int = sum((1 if x == "0" else 0 for x in flat_layer))

        if count_zeros < min_zeros:
            min_ix = i
            min_zeros = count_zeros

    result = 0
    min_layer: str = "".join(
        sorted([item for row in image[min_ix] for item in row]))

    return (min_layer.rindex("1") - min_layer.index("1") +
            1) * (min_layer.rindex("2") - min_layer.index("2") + 1)


def make_image(input_str: str, rows: int, cols: int) -> List[List[int]]:
    image = convert_str_to_layers(input_str, rows, cols)
    new_image: List[List[int]] = [[2 for i in range(cols)]
                                  for r in range(rows)]
    for row in range(rows):
        for col in range(cols):
            for layer in image:
                if layer[row][col] != "2":
                    new_image[row][col] = int(layer[row][col])
                    break

    return new_image


def part2(input_str: str, rows: int, cols: int) -> str:
    image = make_image(input_str, rows, cols)

    for row in image:
        for col in row:
            if col == 0:
                print(" ", end="")
            else:
                print("#", end="")
        print()

    return ""


def main() -> None:
    with open("input_08.txt", "r") as f:
        input_str = f.read()

    # Test cases
    assert make_image("0222112222120000", 2, 2) == [[0, 1], [1, 0]]

    print(part1(input_str, 6, 25))
    print(part2(input_str, 6, 25))


if __name__ == "__main__":
    main()
