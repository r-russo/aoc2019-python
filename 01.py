from typing import List


def fuel_required(mass: int):
    return mass // 3 - 2


def fuel_recursive(mass: int):
    if mass <= 6:
        return 0

    fuel = fuel_required(mass)
    return fuel + fuel_recursive(fuel)


def part1(masses: List[int]):
    return sum(map(fuel_required, masses))


def part2(masses: List[int]):
    return sum(map(fuel_recursive, masses))


if __name__ == "__main__":
    with open("input_01.txt", 'r') as f:
        input_str = f.read().splitlines()

    masses = [int(line) for line in input_str]

    # Test cases
    assert fuel_required(12) == 2
    assert fuel_required(14) == 2
    assert fuel_required(1969) == 654
    assert fuel_required(100756) == 33583
    assert fuel_recursive(14) == 2
    assert fuel_recursive(1969) == 966
    assert fuel_recursive(100756) == 50346

    print(part1(masses))
    print(part2(masses))
