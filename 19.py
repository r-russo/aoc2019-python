from intcode_computer import IntcodeComputer


def simulate_particle(x: int, y: int, program: list[int]) -> int:
    c = IntcodeComputer(program)
    c.add_inputs(x, y)
    c.run()

    return c.out


def simulate_tractor_beam(size: int, program: list[int]) -> None:
    tractor_beam = [["." for _ in range(size)] for _ in range(size)]
    for i in range(size):
        for j in range(size):
            particle = simulate_particle(j, i, program)
            if particle:
                tractor_beam[i][j] = "#"

    tractor_beam_str = "\n".join(["".join(row) for row in tractor_beam])
    print(tractor_beam_str)


def find_particle_in_row(x: int,
                         y: int,
                         program: list[int],
                         first: bool = True,
                         max_x: int = -1) -> int:
    last_particle = 0
    while True:
        particle = simulate_particle(x, y, program)

        if first and last_particle == 0 and particle == 1:
            return x
        if not first and last_particle == 1 and particle == 0:
            return x - 1
        if x > 10 * y:
            break
        if max_x != -1 and x >= max_x:
            if particle:
                return max_x
            else:
                break
        x += 1
        last_particle = particle

    return -1


def count_affected(size: int, program: list[int]) -> int:
    first: int = 0
    last: int = 0
    count: int = 0
    for i in range(size):
        first = find_particle_in_row(first, i, program, max_x=size - 1)
        last = find_particle_in_row(last,
                                    i,
                                    program,
                                    first=False,
                                    max_x=size - 1)

        if first != -1 and last != -1 and last >= first:
            count += last - first + 1

    return count


def find_square(size: int, program: list[int]) -> tuple[int, int]:
    i = size
    min_width: int = size // 2
    max_width: int = size // 2
    while True:
        min_width = find_particle_in_row(min_width, i, program)
        max_width = find_particle_in_row(max_width,
                                         i - size + 1,
                                         program,
                                         first=False)

        if max_width - min_width + 1 == size:
            return i - size + 1, min_width

        i += 1


def part1(program: list[int]) -> int:
    return count_affected(50, program)


def part2(program: list[int]) -> int:
    row, col = find_square(100, program)

    return col * 10000 + row


def main() -> None:
    with open("input_19.txt", "r") as f:
        input_txt = f.read().strip()

    program = [int(x) for x in input_txt.split(",")]

    print(part1(program))
    print(part2(program))


if __name__ == "__main__":
    main()
