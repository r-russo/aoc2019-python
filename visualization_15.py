from typing import Optional

import pygame

COLOR_BG = (0, 0, 0)
COLOR_VISITED = (0, 255, 0)
COLOR_WALL = (255, 0, 0)
COLOR_DROID = (0, 0, 255)
COLOR_START = (255, 255, 255)
COLOR_DESTINATION = (255, 0, 255)
CELL_SIZE = 16


class Map:
    def __init__(self, w: int = 1280, h: int = 720):
        pygame.init()
        self.size = (w, h)
        self.screen = pygame.display.set_mode(self.size)
        self.walls: list[tuple[int, int]] = []
        self.visited: list[tuple[int, int]] = []
        self.position: tuple[int, int] = (0, 0)
        self.centerx = self.size[0] // 2
        self.centery = self.size[1] // 2

        self.destination: Optional[tuple[int, int]] = None

    def update(self) -> bool:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False

        self.screen.fill(COLOR_BG)
        for wall in self.walls:
            x, y = wall
            rect = pygame.Rect(x * CELL_SIZE + self.centerx,
                               y * CELL_SIZE + self.centery, CELL_SIZE,
                               CELL_SIZE)
            pygame.draw.rect(self.screen, COLOR_WALL, rect)

        for visited in self.visited:
            x, y = visited
            rect = pygame.Rect(x * CELL_SIZE + self.centerx,
                               y * CELL_SIZE + self.centery, CELL_SIZE,
                               CELL_SIZE)
            pygame.draw.rect(self.screen, COLOR_VISITED, rect)

        rect = pygame.Rect(self.centerx, self.centery, CELL_SIZE, CELL_SIZE)
        pygame.draw.rect(self.screen, COLOR_START, rect)

        if self.destination is not None:
            x, y = self.destination
            rect = pygame.Rect(x * CELL_SIZE + self.centerx,
                               y * CELL_SIZE + self.centery, CELL_SIZE,
                               CELL_SIZE)
            pygame.draw.rect(self.screen, COLOR_DESTINATION, rect)

        x, y = self.position
        rect = pygame.Rect(x * CELL_SIZE + self.centerx,
                           y * CELL_SIZE + self.centery, CELL_SIZE, CELL_SIZE)
        pygame.draw.rect(self.screen, COLOR_DROID, rect)

        pygame.display.flip()

        return True

    def add_wall(self, position: tuple[int, int]) -> None:
        self.walls.append(position)

    def add_visited(self, position: tuple[int, int]) -> None:
        self.visited.append(position)

    def add_destination(self, position: tuple[int, int]) -> None:
        self.destination = position

    def update_position(self, position: tuple[int, int]) -> None:
        self.position = position
