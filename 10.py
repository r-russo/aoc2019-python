from collections import Counter
from math import atan2, pi, sqrt
from typing import Dict, List, Tuple


def asteroids_coordinates(input_str: str) -> List[Tuple[int, int]]:
    asteroids: List[Tuple[int, int]] = []
    for y, row in enumerate(input_str.splitlines()):
        for x, col in enumerate(row):
            if col == "#":
                asteroids.append((x, y))

    return asteroids


def count_asteroids_in_line_of_sight(asteroids: List[Tuple[int, int]], px: int,
                                     py: int) -> int:
    angles: List[float] = []
    for x, y in asteroids:
        if px == x and py == y:
            continue
        angle = atan2(y - py, x - px)
        if angle not in angles:
            angles.append(angle)

    return len(angles)


def find_best_location(input_str: str) -> Tuple[int, Tuple[int, int]]:
    max_count: int = 0
    best_position: Tuple[int, int]
    asteroids = asteroids_coordinates(input_str)
    for x, y in asteroids_coordinates(input_str):
        count = count_asteroids_in_line_of_sight(asteroids, x, y)
        if count > max_count:
            max_count = count
            best_position = (x, y)

    return max_count, best_position


def asteroids_angles(input_str: str, px: int,
                     py: int) -> Dict[float, List[Tuple[int, int]]]:
    asteroids = asteroids_coordinates(input_str)
    angles: Dict[float, List[Tuple[int, int]]] = {}
    for x, y in asteroids:
        if x == px and y == py:
            continue
        angle = (atan2(y - py, x - px) + pi / 2) % (2 * pi)
        if angle not in angles:
            angles[angle] = []
        angles[angle].append((x, y))

    return angles


def distance(args: Tuple[int, ...], point: Tuple[int, ...]) -> float:
    return sqrt(sum([(c - x)**2 for c, x in zip(args, point)]))


def vaporize(input_str: str, px: int, py: int,
             pos: int) -> List[Tuple[int, int]]:
    asteroids = asteroids_angles(input_str, px, py)
    angles = sorted(asteroids)
    vaporized: List[Tuple[int, int]] = []
    ix = 0
    for _ in range(pos):
        if not asteroids[angles[ix]]:
            continue
        to_vaporize = min(asteroids[angles[ix]],
                          key=lambda p: distance(p, (px, py)))
        vaporized_ix = asteroids[angles[ix]].index(to_vaporize)
        vaporized.append(to_vaporize)
        del asteroids[angles[ix]][vaporized_ix]
        pos -= 1
        ix += 1
        ix %= len(angles)

    return vaporized


def part1(input_str: str) -> int:
    return find_best_location(input_str)[0]


def part2(input_str: str) -> int:
    _, (px, py) = find_best_location(input_str)
    vx, vy = vaporize(input_str, px, py, 200)[-1]
    return vx * 100 + vy


def test_cases() -> None:
    input_str = ".#..#\n.....\n#####\n....#\n...##"
    asteroids = asteroids_coordinates(input_str)
    assert count_asteroids_in_line_of_sight(asteroids, 3, 4) == 8
    input_str = "......#.#.\n#..#.#....\n..#######.\n.#.#.###..\n" \
                ".#..#.....\n..#....#.#\n#..#....#.\n.##.#..###\n" \
                "##...#..#.\n.#....####"
    asteroids = asteroids_coordinates(input_str)
    assert count_asteroids_in_line_of_sight(asteroids, 5, 8) == 33
    input_str = "#.#...#.#.\n.###....#.\n.#....#...\n##.#.#.#.#\n" \
                "....#.#.#.\n.##..###.#\n..#...##..\n..##....##\n" \
                "......#...\n.####.###."
    asteroids = asteroids_coordinates(input_str)
    assert count_asteroids_in_line_of_sight(asteroids, 1, 2) == 35
    input_str = ".#..#..###\n####.###.#\n....###.#.\n..###.##.#\n" \
                "##.##.#.#.\n....###..#\n..#.#..#.#\n#..#.#.###\n" \
                ".##...##.#\n.....#.#.."
    asteroids = asteroids_coordinates(input_str)
    assert count_asteroids_in_line_of_sight(asteroids, 6, 3) == 41
    input_str = ".#..##.###...#######\n##.############..##.\n" \
                ".#.######.########.#\n.###.#######.####.#.\n" \
                "#####.##.#.##.###.##\n..#####..#.#########\n" \
                "####################\n#.####....###.#.#.##\n" \
                "##.#################\n#####.##.###..####..\n" \
                "..######..##.#######\n####.##.####...##..#\n" \
                ".#####..#.######.###\n##...#.##########...\n" \
                "#.##########.#######\n.####.#.###.###.#.##\n" \
                "....##.##.###..#####\n.#.#.###########.###\n" \
                "#.#.#.#####.####.###\n###.##.####.##.#..##\n"
    asteroids = asteroids_coordinates(input_str)
    assert count_asteroids_in_line_of_sight(asteroids, 11, 13) == 210
    vaporized = vaporize(input_str, 11, 13, 200)
    assert vaporized[0] == (11, 12)
    assert vaporized[1] == (12, 1)
    assert vaporized[2] == (12, 2)
    assert vaporized[9] == (12, 8)
    assert vaporized[19] == (16, 0)
    assert vaporized[49] == (16, 9)
    assert vaporized[99] == (10, 16)
    assert vaporized[198] == (9, 6)
    assert vaporized[199] == (8, 2)


def main() -> None:
    test_cases()
    with open("input_10.txt", 'r') as f:
        input_str = f.read()
    print(part1(input_str))
    print(part2(input_str))


if __name__ == "__main__":
    main()
