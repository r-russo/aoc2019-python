def fft(input_sequence: list[int], offset: int = 0) -> list[int]:
    output_sequence: list[int] = [
        0 for _ in range(len(input_sequence) - offset)
    ]
    if len(input_sequence) < 2 * offset:
        acc: int = 0
        for k in range(len(input_sequence) - 1, offset - 1, -1):
            acc += input_sequence[k]

            output_sequence[k - offset] = abs(acc) % 10

        return output_sequence

    for k in range(offset, len(input_sequence)):
        value: int = 0
        i: int = k
        rango = k + 1
        signo: int = 1
        while i < len(input_sequence):
            value += signo * sum(input_sequence[i:i + rango])
            signo *= -1
            i += 2 * rango

        output_sequence[k - offset] = abs(value) % 10

    return output_sequence


def fft_phases(input_signal: str, phases: int) -> int:
    input_sequence = [int(x) for x in input_signal]

    for _ in range(phases):
        input_sequence = fft(input_sequence)

    output = [str(x) for x in input_sequence]

    return int("".join(output))


def fft_phases_with_offset(input_signal: str, phases: int = 100) -> str:
    offset = int(input_signal[:7])

    input_sequence = [int(input_signal[i]) for i in range(len(input_signal))]
    for p in range(phases):
        input_sequence[offset:] = fft(input_sequence, offset)

    output = "".join([str(x) for x in input_sequence])

    return output[offset:offset + 8]


def test_cases() -> None:
    assert str(fft_phases("80871224585914546619083218645595",
                          100))[:8] == "24176176"
    assert str(fft_phases("19617804207202209144916044189917",
                          100))[:8] == "73745418"
    assert str(fft_phases("69317163492948606335995924319873",
                          100))[:8] == "52432133"
    assert fft_phases_with_offset("03036732577212944063491565474664" *
                                  10000) == "84462026"
    assert fft_phases_with_offset("02935109699940807407585447034323" *
                                  10000) == "78725270"
    assert fft_phases_with_offset("03081770884921959731165446850517" *
                                  10000) == "53553731"


def part1(input_txt: str) -> str:
    return str(fft_phases(input_txt, 100))[:8]


def part2(input_txt: str) -> str:
    return str(fft_phases_with_offset(input_txt * 10000, 100))


def main() -> None:
    test_cases()

    with open("input_16.txt", "r") as f:
        input_txt = f.read().strip()

    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
