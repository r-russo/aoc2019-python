from itertools import permutations
from typing import Iterator, List, Optional

from intcode_computer import IntcodeComputer


def amplify(program: List[int],
            phase_settings: tuple[int, ...],
            feedback: bool = False) -> int:
    amp: int = 0
    exec_code: int
    if not feedback:
        for phase in phase_settings:
            computer = IntcodeComputer(program)
            computer.add_inputs(phase, amp)
            if computer.run() == 2:
                amp = computer.out
    else:
        computers = [
            IntcodeComputer(program) for i in range(len(phase_settings))
        ]
        for i, phase in enumerate(phase_settings):
            computers[i].add_inputs(phase)
        computers[0].add_inputs(0)
        i = 0
        while False in [c.finished for c in computers]:
            if computers[i].finished:
                continue
            exec_code = computers[i].run()
            if exec_code == 2:
                computers[(i + 1) % len(computers)].add_inputs(
                    computers[i].out)
            i = (i + 1) % len(computers)

        amp = computers[-1].out

    return amp


def part1(program: List[int]) -> int:
    max_amp: int = 0
    phase_settings: Iterator[tuple[int, ...]] = permutations(range(5), 5)

    amp: int
    for setting in phase_settings:
        amp = amplify(program, setting)
        max_amp = max(max_amp, amp)

    return max_amp


def part2(program: List[int]) -> int:
    amp: int
    max_amp: int = 0
    phase_settings: Iterator[tuple[int, ...]] = permutations(range(5, 10), 5)

    for setting in phase_settings:
        amp = amplify(program, setting, feedback=True)
        max_amp = max(max_amp, amp)

    return max_amp


def main() -> None:
    with open("input_07.txt", "r") as f:
        program = [int(x) for x in f.read().strip("\n").split(",")]

    # Test cases
    # fmt: off
    assert amplify(
        [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0],
        (4, 3, 2, 1, 0)) == 43210
    assert amplify([
        3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1,
        24, 23, 23, 4, 23, 99, 0, 0
    ], (0, 1, 2, 3, 4)) == 54321
    assert amplify([
        3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33,
        1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0
    ], (1, 0, 4, 3, 2)) == 65210
    assert amplify([
        3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27,
        1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5
    ], (9, 8, 7, 6, 5),
                   feedback=True) == 139629729
    assert amplify([
        3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005,
        55, 26, 1001, 54, -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55,
        1001, 55, 1, 55, 2, 53, 55, 53, 4, 53, 1001, 56, -1, 56, 1005, 56, 6,
        99, 0, 0, 0, 0, 10
    ], (9, 7, 8, 5, 6),
                   feedback=True) == 18216

    # fmt: on
    print(part1(program))
    print(part2(program))


if __name__ == "__main__":
    main()
