from collections import deque
from dataclasses import dataclass, field
from typing import Optional

Position = tuple[int, int, int]
Position_steps = tuple[int, int, int, int]


@dataclass(frozen=True)
class Bots:
    x: tuple[int, ...]
    y: tuple[int, ...]


def chr_to_key(cell: str) -> int:
    return int(2**(ord(cell) - ord("a")))


class Maze:
    def __init__(self, map_str: str):
        self.map = [list(x) for x in map_str.strip().splitlines()]
        self.height = len(self.map)
        self.width = len(self.map[0])
        self.keys = {x for x in map_str.strip() if x.islower()}
        self.doors = {x for x in map_str.strip() if x.isupper()}

    def get_cell(self, x: int, y: int) -> str:
        if 0 <= x < self.width and 0 <= y < self.height:
            return self.map[y][x]
        return "#"

    def set_cell(self, x: int, y: int, shape: str) -> None:
        if len(shape) != 1:
            raise ValueError("Should be one character")
        if 0 <= x < self.width and 0 <= y < self.height:
            self.map[y][x] = shape
        else:
            raise IndexError("Coordinate not found")

    def find_cell(self, c: str) -> list[tuple[int, int]]:
        result: list[tuple[int, int]] = []
        for i, row in enumerate(self.map):
            matches = [(j, i) for j, x in enumerate(row) if x == c]
            if matches:
                result.extend(matches)

        return result

    def find_first_cell(self, c: str) -> Optional[tuple[int, int]]:
        for i, row in enumerate(self.map):
            matches = [j for j, x in enumerate(row) if x == c]
            if matches:
                return (matches[0], i)

        return None

    def get_available_neighbours(self, x: int, y: int,
                                 keys: int) -> list[tuple[int, int, int]]:
        up = (x, y - 1)
        right = (x + 1, y)
        down = (x, y + 1)
        left = (x - 1, y)

        available: list[tuple[int, int, int]] = []

        for (nx, ny) in (up, right, down, left):
            cell = self.get_cell(nx, ny)
            if cell in (".", "@"):
                available.append((nx, ny, -1))
            elif cell.isupper() and chr_to_key(cell.lower()) & keys:
                available.append((nx, ny, -1))
            elif cell.islower():
                available.append((nx, ny, chr_to_key(cell)))

        return available

    def get_neighbours(self, x: int, y: int) -> list[tuple[int, int, str]]:
        up = (x, y - 1)
        right = (x + 1, y)
        down = (x, y + 1)
        left = (x - 1, y)

        neighbours: list[tuple[int, int, str]] = []

        for (nx, ny) in (up, right, down, left):
            cell = self.get_cell(nx, ny)
            if cell == "#":
                continue
            neighbours.append((nx, ny, cell))

        return neighbours

    def __str__(self) -> str:
        return "\n".join(["".join(row) for row in self.map])

    def __repr__(self) -> str:
        return str(self)


def bfs(maze: Maze) -> int:
    visited: set[Position] = set()
    starting_position = maze.find_first_cell("@")
    if starting_position is None:
        raise IndexError("Starting position doesn't exist?")
    queue: deque[Position_steps] = deque()
    p = (*starting_position, 0, 0)
    queue.appendleft(p)
    visited.add((*starting_position, 0))
    total_keys = 2**len(maze.keys) - 1

    while queue:
        x, y, keys, steps = queue.pop()
        if keys == total_keys:
            break

        for (nx, ny, key) in maze.get_available_neighbours(x, y, keys):
            new_keys = keys
            if key >= 0:
                new_keys |= key
            if (nx, ny, new_keys) not in visited:
                visited.add((nx, ny, new_keys))
                queue.appendleft((nx, ny, new_keys, steps + 1))

    return steps


def bfs_bots(maze: Maze, bots_position: list[tuple[int, int]]) -> int:
    visited: set[tuple[Bots, int]] = set()
    queue: deque[tuple[Bots, int, int, int]] = deque()

    sx: list[int] = []
    sy: list[int] = []
    for pos in bots_position:
        sx.append(pos[0])
        sy.append(pos[1])

    visited.add((Bots(tuple(sx), tuple(sy)), 0))
    for i in range(4):
        queue.appendleft((Bots(tuple(sx), tuple(sy)), 0, i, 0))

    total_keys = 2**len(maze.keys) - 1

    while queue:
        bots, keys, moving_bot, steps = queue.pop()

        if keys == total_keys:
            break

        bx = bots.x[moving_bot]
        by = bots.y[moving_bot]
        for (x, y, key) in maze.get_available_neighbours(bx, by, keys):
            new_keys = keys
            if key >= 0:
                new_keys |= key
            new_x = list(bots.x)
            new_x[moving_bot] = x
            new_y = list(bots.y)
            new_y[moving_bot] = y
            new_b = Bots(tuple(new_x), tuple(new_y))
            if (new_b, new_keys) not in visited:
                visited.add((new_b, new_keys))

                if new_keys != keys:
                    for i in range(4):
                        queue.appendleft((new_b, new_keys, i, steps + 1))
                else:
                    queue.appendleft((new_b, new_keys, moving_bot, steps + 1))

    return steps


def test_cases() -> None:
    input_txt = """#########
#b.A.@.a#
#########"""
    assert bfs(Maze(input_txt)) == 8

    input_txt = """########################
#f.D.E.e.C.b.A.@.a.B.c.#
######################.#
#d.....................#
########################"""
    assert bfs(Maze(input_txt)) == 86

    input_txt = """########################
#...............b.C.D.f#
#.######################
#.....@.a.B.c.d.A.e.F.g#
########################"""
    assert bfs(Maze(input_txt)) == 132

    input_txt = """#################
#i.G..c...e..H.p#
########.########
#j.A..b...f..D.o#
########@########
#k.E..a...g..B.n#
########.########
#l.F..d...h..C.m#
#################"""
    assert bfs(Maze(input_txt)) == 136

    input_txt = """########################
#@..............ac.GI.b#
###d#e#f################
###A#B#C################
###g#h#i################
########################"""
    assert bfs(Maze(input_txt)) == 81

    input_txt = """#######
#a.#Cd#
##@#@##
#######
##@#@##
#cB#Ab#
#######"""
    m = Maze(input_txt)
    assert bfs_bots(m, m.find_cell("@")) == 8

    input_txt = """###############
#d.ABC.#.....a#
######@#@######
###############
######@#@######
#b.....#.....c#
###############"""
    m = Maze(input_txt)
    assert bfs_bots(m, m.find_cell("@")) == 24

    input_txt = """#############
#DcBa.#.GhKl#
#.###@#@#I###
#e#d#####j#k#
###C#@#@###J#
#fEbA.#.FgHi#
#############"""
    m = Maze(input_txt)
    assert bfs_bots(m, m.find_cell("@")) == 32

    input_txt = """#############
#g#f.D#..h#l#
#F###e#E###.#
#dCba@#@BcIJ#
#############
#nK.L@#@G...#
#M###N#H###.#
#o#m..#i#jk.#
#############"""
    m = Maze(input_txt)
    assert bfs_bots(m, m.find_cell("@")) == 72


def part1(input_txt: str) -> int:
    m = Maze(input_txt)
    return bfs(m)


def part2(input_txt: str) -> int:
    m = Maze(input_txt)
    starting_position = m.find_first_cell("@")
    if starting_position is None:
        raise IndexError("Starting position doesn't exist?")
    sx, sy = starting_position

    for i in range(-1, 2):
        for j in range(-1, 2):
            if i in (-1, 1) and j in (-1, 1):
                m.set_cell(j + sx, i + sy, "@")
            else:
                m.set_cell(j + sx, i + sy, "#")

    return bfs_bots(m, m.find_cell("@"))


def main() -> None:
    test_cases()

    with open("input_18.txt", "r") as f:
        input_txt = f.read().strip()

    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
