from itertools import combinations

from intcode_computer import IntcodeComputer


def part1(program: list[int]) -> int:
    c = IntcodeComputer(program)

    exec_code = c.run()
    output = ""
    commands: list[str] = [
        "west",
        "south",
        "south",
        "south",
        "take asterisk",
        "north",
        "north",
        "north",
        "west",
        "west",
        "south",
        "take fixed point",
        "west",
        "take food ration",
        "east",
        "north",
        "west",
        "take dark matter",
        "east",
        "east",
        "south",
        "take astronaut ice cream",
        "south",
        "take polygon",
        "east",
        "take easter egg",
        "east",
        "take weather machine",
        "north",
    ][::-1]
    items = [
        "asterisk", "fixed point", "food ration", "dark matter",
        "astronaut ice cream", "polygon", "easter egg", "weather machine"
    ]
    permutations: list[tuple[str, ...]] = []
    for i in range(1, 9):
        permutations.extend(list(combinations(items, i)))
    while exec_code != 0:
        if exec_code == 1:
            if commands:
                command = commands.pop()
            else:
                command = input()

            if command.startswith("next"):
                for perm in permutations:
                    for it in items:
                        command = f"drop {it}"
                        c.add_inputs(*[ord(x) for x in command], 10)
                    for it in perm:
                        command = f"take {it}"
                        c.add_inputs(*[ord(x) for x in command], 10)
                    c.add_inputs(*[ord(x) for x in "north"], 10)
            else:
                inputs = [ord(x) for x in command]
            c.add_inputs(*inputs, 10)
        elif exec_code == 2:
            if c.out == 10:
                print(output)
                output = ""
            else:
                output += chr(c.out)

        exec_code = c.run()


def main() -> None:
    with open("input_25.txt", "r") as f:
        input_txt = f.read().strip()

    program = [int(x) for x in input_txt.split(',')]

    print(part1(program))


if __name__ == "__main__":
    main()
