from typing import Any, Dict, List

test_input = """
COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
"""


def make_orbits(orbits: str) -> Dict:
    graph: Dict[str, str] = {}
    for line in orbits.splitlines():
        if not ")" in line:
            continue
        around, in_orbit = line.split(")")
        if in_orbit in graph:
            raise ValueError
        graph[in_orbit] = around

    return graph


def count_orbits(orbits: Dict[str, str]) -> int:
    count = 0
    for current_orbit in orbits:
        count += 1
        next_orbit = orbits[current_orbit]
        while next_orbit != "COM":
            count += 1
            next_orbit = orbits[next_orbit]

    return count


def count_orbits_between(orbits: Dict[str, str], from_orbit: str,
                         to_orbit: str) -> int:
    count: int = 0

    # go from YOU to COM
    path_from: List[str] = []
    next_orbit: str = orbits[from_orbit]
    while next_orbit != "COM":
        path_from.append(next_orbit)
        next_orbit = orbits[next_orbit]

    # go from SAN to COM
    path_to: List[str] = []
    next_orbit = orbits[to_orbit]
    while next_orbit != "COM":
        path_to.append(next_orbit)
        next_orbit = orbits[next_orbit]

    for node in path_from:
        if node in path_to:
            break
        count += 1

    last_node = node
    for node in path_to:
        if node == last_node:
            break
        count += 1

    return count


def part1(input_str: str) -> int:
    graph = make_orbits(input_str)
    return count_orbits(graph)


def part2(input_str: str) -> int:
    graph = make_orbits(input_str)
    return count_orbits_between(graph, "YOU", "SAN")


if __name__ == "__main__":
    with open("input_06.txt", "r") as f:
        input_str = f.read()

    # Test cases
    assert part1(test_input) == 42
    test_input += """K)YOU\nI)SAN"""
    assert part2(test_input)

    print(part1(input_str))
    print(part2(input_str))
