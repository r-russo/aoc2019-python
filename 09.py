from typing import List

from intcode_computer import IntcodeComputer


def test_cases() -> None:
    computer = IntcodeComputer([
        109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0,
        99
    ])
    exec_code: int = computer.run()
    out: List[int] = []
    while exec_code != 0:
        if exec_code == 2:
            out.append(computer.out)
        exec_code = computer.run()
    assert out == [
        109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0,
        99
    ]

    computer = IntcodeComputer([1102, 34915192, 34915192, 7, 4, 7, 99, 0])
    if computer.run() == 2:
        assert computer.out > 2**15

    computer = IntcodeComputer([104, 1125899906842624, 99])
    if computer.run() == 2:
        assert computer.out == 1125899906842624


def part1(program: List[int]) -> int:
    computer = IntcodeComputer(program)
    computer.add_inputs(1)
    exec_code: int = computer.run()
    while exec_code != 0:
        if exec_code == 2:
            out: int = computer.out
        exec_code = computer.run()
    return out


def part2(program: List[int]) -> int:
    computer = IntcodeComputer(program)
    computer.add_inputs(2)
    exec_code: int = computer.run()
    while exec_code != 0:
        if exec_code == 2:
            out: int = computer.out
        exec_code = computer.run()
    return out


def main() -> None:
    test_cases()

    with open("input_09.txt", "r") as f:
        program = [int(x) for x in f.read().strip("\n").split(",")]

    print(part1(program))
    print(part2(program))


if __name__ == "__main__":
    main()
