from intcode_computer import IntcodeComputer


def is_intersection(view_list: list[list[str]], i: int, j: int) -> bool:
    return view_list[i][j] == "#" and view_list[i + 1][j] == "#" and \
        view_list[i - 1][j] == "#" and view_list[i][j+1] == "#" and \
        view_list[i][j - 1] == "#"


class Bot:
    def __init__(self, position: tuple[int, int], direction: tuple[int, int]):
        self.position = position
        self.direction = direction
        self.path: str = ""

    def rotate(self) -> None:
        self.direction = (self.direction[1] * -1, self.direction[0])
        self.path += "R,"

    def forward(self, amount: int = 1) -> None:
        self.position = (self.position[0] + self.direction[0] * amount,
                         self.position[1] + self.direction[1] * amount)
        self.path += f"{amount},"

    def ahead(self) -> tuple[int, int]:
        return (self.position[0] + self.direction[0],
                self.position[1] + self.direction[1])

    def simplify_path(self) -> str:
        new_path = ""
        forward: int = 0
        count_same_rotation: int = 0
        split_path = self.path.split(",")
        for current_char, next_char in zip(split_path, split_path[1:]):
            if current_char.isdigit():
                forward += 1
                if not next_char.isdigit():
                    new_path += f"{forward},"
                    forward = 0
            elif current_char == "R":
                count_same_rotation += 1
                if next_char != "R":
                    if count_same_rotation > 2:
                        new_path += f"{'L,' * (4 - count_same_rotation)}"
                    else:
                        new_path += f"{'R,' * count_same_rotation}"
                    count_same_rotation = 0
            else:
                new_path += f"{current_char},"

        return new_path

    def get_path(self, simplify: bool = True) -> str:
        if simplify:
            return self.simplify_path().strip(",")
        return self.path.strip(",")


class View:
    view: str
    intersections: list[tuple[int, int]]
    view_list: list[list[str]]

    def generate_view(self, program: list[int]) -> None:
        c = IntcodeComputer(program)
        exec_code: int = c.run()
        view: str = ""

        while exec_code != 0:
            if exec_code == 2:
                view += chr(c.out)
            exec_code = c.run()

        self.set_view(view)

    def get_cell(self, x: int, y: int) -> str:
        if 0 <= y < len(self.view_list) and 0 <= x < len(self.view_list[y]):
            return self.view_list[y][x]
        return "."

    def can_move(self, x: int, y: int) -> bool:
        return self.get_cell(x + 1, y) == "#" or \
            self.get_cell(x - 1, y) == "#" or \
            self.get_cell(x, y + 1) == "#" or \
            self.get_cell(x, y - 1) == "#"

    def set_view(self, view: str) -> None:
        self.intersections = []
        self.view_list = [list(x) for x in view.strip().splitlines()]

    def find_intersections(self) -> None:
        self.intersections = []
        for i, row in enumerate(self.view_list):
            if i in (0, len(self.view_list) - 1):
                continue

            for j in range(len(row)):
                if j in (0, len(row) - 1):
                    continue

                if is_intersection(self.view_list, i, j):
                    self.intersections.append((j, i))

    def get_alignment_parameters(self) -> int:
        if not self.intersections:
            self.find_intersections()
        parameter: int = 0

        for intersection in self.intersections:
            parameter += intersection[0] * intersection[1]

        return parameter

    def get_path(self) -> str:
        for i, row in enumerate(self.view_list):
            for j, col in enumerate(row):
                if col == "^":
                    bot_position = (j, i)
                    bot_direction = (0, -1)
                elif col == ">":
                    bot_position = (j, i)
                    bot_direction = (1, 0)
                elif col == "<":
                    bot_position = (j, i)
                    bot_direction = (-1, 0)
                elif col == "v":
                    bot_position = (j, i)
                    bot_direction = (0, 1)
                else:
                    continue
                break
            else:
                continue

        bot = Bot(bot_position, bot_direction)

        if not self.intersections:
            self.find_intersections()

        while True:
            if not self.can_move(*bot.position):
                break

            while self.get_cell(*bot.ahead()) != "#":
                bot.rotate()

            while self.get_cell(*bot.ahead()) == "#":
                j, i = bot.position
                if bot.position not in self.intersections:
                    self.view_list[i][j] = "X"
                bot.forward()

        return bot.get_path()

    def __str__(self) -> str:
        return "\n".join(["".join(x) for x in self.view_list])

    def __repr__(self) -> str:
        return str(self)


def part1(input_txt: str) -> int:
    program = [int(x) for x in input_txt.split(",")]

    view = View()
    view.generate_view(program)
    return view.get_alignment_parameters()


def part2(input_txt: str) -> int:
    program = [int(x) for x in input_txt.split(",")]

    view = View()
    view.generate_view(program)
    path = view.get_path()

    program = [int(x) for x in input_txt.split(",")]
    program[0] = 2

    func_A = "R,8,L,12,R,8\n"
    func_B = "R,12,L,8,R,10\n"
    func_C = "R,8,L,8,L,8,R,8,R,10\n"
    routine = "A,B,B,A,C,A,A,C,B,C\n"
    feed = "n\n"

    str_input = routine + func_A + func_B + func_C + feed

    inputs = [ord(x) for x in str_input]

    computer = IntcodeComputer(program)
    computer.add_inputs(*inputs)
    exec_code: int = computer.run()

    while exec_code != 0:
        exec_code = computer.run()

    return computer.out


def test_cases() -> None:
    view = View()
    view_str = """..#..........
..#..........
#######...###
#.#...#...#.#
#############
..#...#...#..
..#####...^.."""
    view.set_view(view_str)
    assert view.get_alignment_parameters() == 76

    view_str = """#######...#####
#.....#...#...#
#.....#...#...#
......#...#...#
......#...###.#
......#.....#.#
^########...#.#
......#.#...#.#
......#########
........#...#..
....#########..
....#...#......
....#...#......
....#...#......
....#####......"""
    view.set_view(view_str)

    assert view.get_path(
    ) == "R,8,R,8,R,4,R,4,R,8,L,6,L,2,R,4,R,4,R,8,R,8,R,8,L,6,L,2"


def main() -> None:
    test_cases()

    with open("input_17.txt", "r") as f:
        input_txt = f.read().strip()

    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
