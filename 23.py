from collections import deque

from typing import Optional
from intcode_computer import IntcodeComputer


def part1(input_txt: str) -> int:
    program = [int(x) for x in input_txt.split(",")]
    computers = [IntcodeComputer(program) for _ in range(50)]

    for i, c in enumerate(computers):
        c.add_inputs(i)

    packet_queue: deque[tuple[int, int, int]] = deque()

    while True:
        for i, c in enumerate(computers):
            exec_code = c.run()
            if exec_code == 1:
                if not packet_queue or packet_queue[0][0] != i:
                    c.add_inputs(-1)
                else:
                    packet = packet_queue.popleft()
                    c.add_inputs(packet[1], packet[2])
            elif exec_code == 2:
                addr = c.out
                c.run()
                x = c.out
                c.run()
                y = c.out
                packet_queue.append((addr, x, y))
                if addr == 255:
                    return y


def part2(input_txt: str) -> int:
    program = [int(x) for x in input_txt.split(",")]
    computers = [IntcodeComputer(program) for _ in range(50)]

    for i, c in enumerate(computers):
        c.add_inputs(i)

    packet_queue: deque[tuple[int, int, int]] = deque()
    nat: Optional[tuple[int, int]] = None
    last_y: int = -1

    while True:
        idle = True
        for i, c in enumerate(computers):
            exec_code = c.run()
            if exec_code == 1:
                if not packet_queue or packet_queue[0][0] != i:
                    c.add_inputs(-1)
                else:
                    packet = packet_queue.popleft()
                    c.add_inputs(packet[1], packet[2])
            elif exec_code == 2:
                idle = False
                addr = c.out
                c.run()
                x = c.out
                c.run()
                y = c.out
                if addr == 255:
                    nat = (x, y)
                else:
                    packet_queue.append((addr, x, y))

        if idle and not packet_queue and nat is not None:
            if last_y == nat[-1]:
                return last_y
            computers[0].add_inputs(*nat)
            last_y = nat[-1]


def main() -> None:
    with open("input_23.txt", "r") as f:
        input_txt = f.read().strip()

    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
