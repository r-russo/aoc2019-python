from copy import deepcopy


class Bugs:
    def __init__(self, input_txt: str, recursive: bool = False):
        self.recursive = recursive
        self.levels = {0: [list(x) for x in input_txt.splitlines()]}
        self.width = len(self.levels[0][0])
        self.height = len(self.levels[0])
        self.cx, self.cy = self.width // 2, self.height // 2
        if recursive:
            self.levels[0][self.cy][self.cx] = "?"

        assert not list(
            filter(lambda x: len(x) != len(self.levels[0][0]), self.levels[0]))

    def get_cell(self, x: int, y: int, level: int = 0) -> str:
        if level in self.levels:
            if 0 <= x < self.width and 0 <= y < self.height:
                return self.levels[level][y][x]
        return "."

    def grow(self) -> None:
        keys = self.levels.keys()
        for level in (min(keys) - 1, max(keys) + 1):
            self.levels[level] = [["." for _ in range(self.width)]
                                  for _ in range(self.height)]
            self.levels[level][self.cy][self.cx] = "?"

    def get_neighbours(self, x: int, y: int, level: int = 0) -> list[str]:
        neighbours: list[str] = []

        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            nx, ny = x + dx, y + dy
            if self.recursive:
                if (nx, ny) == (self.cx, self.cy):
                    if (dx, dy) == (1, 0):  # L
                        for i in range(self.width):
                            neighbours.append(self.get_cell(0, i, level + 1))
                    elif (dx, dy) == (-1, 0):  # N
                        for i in range(self.width):
                            neighbours.append(
                                self.get_cell(self.width - 1, i, level + 1))
                    elif (dx, dy) == (0, 1):  # H
                        for i in range(self.height):
                            neighbours.append(self.get_cell(i, 0, level + 1))
                    elif (dx, dy) == (0, -1):  # R
                        for i in range(self.height):
                            neighbours.append(
                                self.get_cell(i, self.height - 1, level + 1))
                elif ny == -1:  # 8
                    neighbours.append(
                        self.get_cell(self.cx, self.cy - 1, level - 1))
                elif ny == self.height:  # 18
                    neighbours.append(
                        self.get_cell(self.cx, self.cy + 1, level - 1))
                elif nx == -1:  # 12
                    neighbours.append(
                        self.get_cell(self.cx - 1, self.cy, level - 1))
                elif nx == self.width:  # 14
                    neighbours.append(
                        self.get_cell(self.cx + 1, self.cy, level - 1))
                else:
                    neighbours.append(self.get_cell(nx, ny, level))
            else:
                neighbours.append(self.get_cell(nx, ny, level))

        return neighbours

    def simulate(self) -> None:
        if self.recursive:
            self.grow()
        new_levels = deepcopy(self.levels)
        for level in self.levels:
            for x in range(self.width):
                for y in range(self.height):
                    cell = self.get_cell(x, y, level)
                    if cell == "?":
                        continue
                    bug = cell == "#"
                    adjacent_bugs = self.get_neighbours(x, y, level).count("#")
                    if bug and adjacent_bugs != 1:
                        new_levels[level][y][x] = "."
                    if not bug and adjacent_bugs in (1, 2):
                        new_levels[level][y][x] = "#"

        self.levels = new_levels

    def score(self) -> int:
        score: int = 0
        for x in range(self.width):
            for y in range(self.height):
                if self.get_cell(x, y) == "#":
                    score += 2**(x + y * self.width)

        return score

    def count_bugs(self) -> int:
        c = 0
        for level in self.levels:
            for row in self.levels[level]:
                c += row.count("#")
        return c

    def __str__(self) -> str:
        return repr(self)

    def __repr__(self) -> str:
        grids = ""
        for level in sorted(self.levels.keys()):
            grids += f"level = {level}\n"
            grids += "\n".join(["".join(line) for line in self.levels[level]])
            grids += "\n"

        return grids


def test_cases() -> None:
    input_txt = """....#
#..#.
#..##
..#..
#...."""
    b = Bugs(input_txt)
    repeated_layouts: set[str] = set()
    repeated_layouts.add(str(b))
    b.simulate()
    while str(b) not in repeated_layouts:
        repeated_layouts.add(str(b))
        b.simulate()
    assert b.score() == 2129920

    input_txt = """....#
#..#.
#.?##
..#..
#...."""
    b = Bugs(input_txt, recursive=True)
    for _ in range(10):
        b.simulate()

    assert b.count_bugs() == 99


def part1(input_txt: str) -> int:
    b = Bugs(input_txt)
    repeated_layouts: set[str] = set()
    repeated_layouts.add(str(b))
    b.simulate()
    while str(b) not in repeated_layouts:
        repeated_layouts.add(str(b))
        b.simulate()
    return b.score()


def part2(input_txt: str) -> int:
    b = Bugs(input_txt, recursive=True)
    for _ in range(200):
        b.simulate()

    return b.count_bugs()


def main() -> None:
    test_cases()

    with open("input_24.txt", "r") as f:
        input_txt = f.read().strip()

    print(part1(input_txt))
    print(part2(input_txt))


if __name__ == "__main__":
    main()
